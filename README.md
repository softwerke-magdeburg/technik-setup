# Softwerke Server-Setup mit Ansible

```
git clone git@codeberg.org:softwerke-magdeburg/technik-setup.git
cd technik-setup
ansible-playbook -i inventory.ini playbook.yml
```

Um einen spezifischen SSH-Key zu benutzen kann `-e ansible_ssh_private_key_file=~/.ssh/id_...` genutzt werden. 

Siehe auch die [Dokumentation](https://codeberg.org/momar/asd-server/wiki/Overview) des Upstream-Projekts.
